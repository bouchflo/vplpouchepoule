from VPL_Web_api import *
import string
import os
from subprocess import Popen
from getch import getche
## requires getch:
# pip install getch


course_id = None
course = None
vpl_id = None
vpl    = None


def set_courseId(i):
    global course_id
    course_id = i

def set_TERMINAL (term):
    global TERMINAL
    TERMINAL = term


def are_you_sure():
    print ("\nDo you really want to proceed? (y/N) ", end='', flush=True)
    ch = getche()
    print()
    if ch != 'y':
        print ("cancelled...")
        return False
    return True


def pull_my_course():
    global course
    """
    Pull a clean version of the Caseine course and writes on disk
    """
    if not course:
        read_course()

    print (course)

    print ("This will erase all local files for this course on your disk.")
    print ("It is recommended to backup your files or commit your work in git,")
    print ("and to delete the 'VPLs/' directory.")

    if not are_you_sure():
        return

    print ("Getting course content from Caseine")
    course.pull_contents ()
    print ("Writing cours on local disk")
    if not os.path.exists(BASEDIR_VPLS):
        os.mkdir(BASEDIR_VPLS)
    course.write_on_disk()

    print ("Sections in the course")
    course.show_sections()
    course.show_vpl_modules()

    print ("Now getting all VPL activities:")
    for v in course.vpl_modules():

        vpl = VPL(v['id'])

        vpl.pull_moodle()
        vpl.write_on_disk()


def updateAllVPLs():
    global course
    """
    Search if there are new VPL activities on the course
    """
    assert(course)
    course.search_new_vpls()
    print ("Update finished, please restart.")
    exit (0)


def checkSelected():
    if not vpl_id:
        print ("Please select a VPL activity first (command 'v').")
        return False
    return True

def checkRead():
    global vpl
    if not checkSelected():
        return
    if not vpl:
        print ("Please read the VPL activity from disk first (command 'r').")
        return False
    return True


def readVPL():
    global vpl
    if not checkSelected(): return

    vpl = VPL(vpl_id)
    vpl.read_from_disk()


def pushVPL():
    if not checkRead(): return
    print("Pushing", vpl.name, "to moodle")
    vpl.push_moodle()



def openVPL():
    if not checkRead(): return
    d = vpl.get_dir()
    # os.system(TERMINAL.format(dir = d))
    term = list(TERMINAL)
    term.append(d)
    p = Popen(term)

def showVPL():
    if not checkRead(): return
    print (vpl.name)

def showFullVPL():
    if not checkRead(): return
    print (vpl)



def updateVPL():
    if not checkRead(): return
    print ("Warning, this will erase all local modifications you may have")
    print ("for this VPL activity.")
    if not are_you_sure():
        return
    vpl.update()


def evaluateVPL():
    if not checkSelected(): return

    vid = vpl_id
    res = call ('mod_vpl_info',
            id = vid
            )
    disp_response (res)


    res = call ('mod_vpl_open',
            id = vid
            )

    disp_response (res)

    res = call ('mod_vpl_evaluate',
                id = vid
            )
    disp_response (res)

    from time import sleep

    sleep(1)

    res = call ('mod_vpl_get_result',
            id = vid
            )

    disp_response (res)



def read_course():
    global course
    if not course_id:
        print ("Course ID is not defined")
    course = Course(courseid = course_id)
    course.read_from_disk()


def display_course():
    global course
    print ("Sections in the course")
    course.show_sections()
    course.show_vpl_modules()






def commandHelp():
    print ("Here is a list of all possible commands:")
    for key,cmd in commands.items():
        print ("\t",key,"\t",cmd['msg'], sep='')


def vpldic2str (v):
    return str(v['id']) + " => " + v['name']

def selectVPL():
    global course
    global vpl

    mods = course.vpl_modules()
    course.show_vpl_modules()

    choice = input ("\nChoose a VPL activity (input the id or part of the name): ")

    choice = choice.lower()

    vpldic = None
    possible = []

    for v in mods:
        if v['id'] == choice:
            vpldic = v
            break
        if v['name'].lower().find(choice) != -1:
            possible.append(v)

    if not vpldic:
        if not possible:
            print ("Error: unknown VPL activity")
        else:
            l = list(map (vpldic2str, possible))
            idx = select_from_list(l)
            if idx != None:
                vpldic = possible[idx]

    if not vpldic:
        print ("No VPL activity selected")
        return

    print ("You selected:", vpldic2str (vpldic))
    global vpl_id
    vpl_id = vpldic['id']



def select_from_list(l):
    """
    Returns the index of the choice or None if no choice
    """
    if len(l) > 26:
        print ("Too many possibilities...")
        print (l)
        return None

    if len(l) == 1:
        return 0

    letters = string.ascii_lowercase
    couples = zip(letters, l)

    for k,n in couples:
        print ("\t",k,"\t",n, sep='')

    print ("\nWhich activity do you want to load? ('X' to cancel)")
    print ("> ",end='', flush=True)

    ch = getche()
    print()

    if ch == 'x' or ch == 'X':
        return None

    return ord(ch) - ord('a')


def quit():
    exit(0)

commands = {
          'h': { 'fun':commandHelp, 'msg': "Display this help message" }
        # , 'l': { 'fun':listVPLActivities, 'msg': "List VPL activities" }
        , 'd': { 'fun':display_course, 'msg': "Display current course" }
        , 'v': { 'fun':selectVPL, 'msg': "Select VPL activity" }
        , 'u': { 'fun':updateVPL, 'msg': "Update current VPL from moodle"}
        , 'U': { 'fun':updateAllVPLs,
                 'msg': "Update list of VPLs from moodle, and pull new ones" }
        , 'o': { 'fun':openVPL, 'msg': "Open a terminal in current VPL activity" }
        , 'r': { 'fun':readVPL, 'msg': "Read VPL activity from disk" }
        , 'p': { 'fun':pushVPL, 'msg': "Push VPL activity to moodle" }
        , 'e': { 'fun':evaluateVPL, 'msg': "Evaluate current VPL activity on moodle" }
        , 's': { 'fun':showVPL, 'msg': "Show name of current VPL activity" }
        , 'S': { 'fun':showFullVPL, 'msg': "Show all of current VPL activity" }
        , 'C': { 'fun':pull_my_course,
                 'msg': "Creates a local view of VPLs for your course by downloading from moodle" }
        , 'q': { 'fun':quit, 'msg': "Quit"}
        }



def launch():
    try :
        read_course()
    except FileNotFoundError:
        print ("Cannot read course", course_id, "from disk. Some files are missing.")
        print ("We recommand to first create a local view of your course")
        print ("using the command 'C'.")

    while True:
        print ("\nCommand to execute: (type 'h' for help)")
        print ("> ",end='', flush=True)

        ch = getche()
        print()

        if ch not in commands:
            print ("Unkown command:", ch)
            continue

        fun = commands[ch]['fun']
        try:
            fun()
        except SystemError as e:
            print ("SystemError detected")
            print (e)




