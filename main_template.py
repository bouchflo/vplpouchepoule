#!/usr/bin/env python3
##################################################
#
# WARNING:
# Copy this file to 'main.py' before editing it
#
##################################################

from VPL_Web_api import *
from interactive_menu import *


# You can find your keys in moodle:
# Go to your personnal preferences, then "Security keys"

my_keys = {
  'Mobile': "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # Moodle mobile web service, for general course functions
, 'VPL':    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # VPL web service, required to push/pull/evaluate VPLs
, 'POV':    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" # Point of View Service, not sure if it is useful,
                                               # you can probably leave it empty
}

set_KEYS (my_keys)

## Your course ID (a number, can be found in the URL of the course on moodle)
## e.g.: https://moodle.caseine.org/course/view.php?id=219
## id would be 219 here
set_courseId(None)

## Terminal command to open a new terminal in the directory of a VPL
set_TERMINAL("gnome-terminal --working-directory='{dir}'")

## Launch the interactive menu
launch()
