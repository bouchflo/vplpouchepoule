##
#
# Florent Bouchez Tichadou
# Inspired by Martin Vuk
# https://github.com/mrcinv/moodle_api.py
#
##

from requests import get, post
import sys
import os
from os import path
import glob
import json
from time import sleep

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

# Module variables to connect to moodle api

def set_KEYS(k):
    global KEYS, VPLwebServiceKEY, MoodleMobileKEY, POVKEY, KEY
    KEYS = k
    VPLwebServiceKEY = KEYS["VPL"]
    MoodleMobileKEY  = KEYS["Mobile"]
    POVKEY = KEYS["POV"]

    # current key
    KEY = VPLwebServiceKEY

BASEDIR_VPLS = "VPLs"

VPL_FILETYPES = {
        "reqfiles" :"mod_vpl_save_required_files",
        "execfiles":"mod_vpl_save_execution_files",
        "corrfiles":"mod_vpl_save_corrected_files"
        }


INFO_FILE = "info.json"

def change_key (opt):
    global KEY
    if opt in KEYS:
        KEY = KEYS[opt]
    else:
        eprint ("Error: Unknown KEY service '" + opt + "'")


# URL = "http://193.55.48.132"  ## serveur de test
URL = "https://moodle.caseine.org"   ## serveur de prod

# URL = "http://193.55.48.129"   ## serveur de prod
ENDPOINT="/webservice/rest/server.php"


def pretty_dict (r):
    """
    Display the dictionnary obtained from json response in a readable manner.
    """
    return json.dumps(r, indent=4)

def disp_response (r):
    print (pretty_dict (r))


def rest_api_parameters(in_args, prefix='', out_dict=None):
    """Transform dictionary/array structure to a flat dictionary, with key names
    defining the structure.

    Example usage:
    >>> rest_api_parameters({'courses':[{'id':1,'name': 'course1'}]})
    {'courses[0][id]':1,
     'courses[0][name]':'course1'}
    """
    if out_dict==None:
        out_dict = {}
    if not type(in_args) in (list,dict):
        out_dict[prefix] = in_args
        return out_dict
    if prefix == '':
        prefix = prefix + '{0}'
    else:
        prefix = prefix + '[{0}]'
    if type(in_args)==list:
        for idx, item in enumerate(in_args):
            rest_api_parameters(item, prefix.format(idx), out_dict)
    elif type(in_args)==dict:
        for key, item in in_args.items():
            rest_api_parameters(item, prefix.format(key), out_dict)
    return out_dict


def call(fname, key = None, **kwargs):
    """Calls moodle API function with function name fname and keyword arguments.

    Example:
    >>> call_mdl_function('core_course_update_courses',
                           courses = [{'id': 1, 'fullname': 'My favorite course'}])
    """

    if key == None:
        key = KEY

    parameters = rest_api_parameters(kwargs)
    parameters.update({"wstoken": key, 'moodlewsrestformat': 'json', "wsfunction": fname})

    ## Problem when there are no files, for instance no "solution" files
    # if not any(kwargs['files']):
        # parameters['files'] = "null"
    # print (parameters)

    print ("Request:", URL+ENDPOINT, fname, end='')
    if 'id' in parameters:
        print (" id:", parameters['id'],sep='', end='')
    if 'key' in parameters:
        print (" key:", parameters['key'],sep='', end='')
    print ()

    import http.client as http_client
    import logging

    http_client.HTTPConnection.debuglevel = 0
    logging.basicConfig()
    logging.getLogger().setLevel(logging.DEBUG)
    requests_log = logging.getLogger("requests.packages.urllib3")
    requests_log.setLevel(logging.DEBUG)
    requests_log.propagate = True

    response = post(URL+ENDPOINT, parameters)
    # print ("response:", response)
    # print ("response url:", response.url)
    # print ("response text:", response.text)
    response = response.json()

    if type(response) == dict and 'exception' in response:
        print ("Error calling Moodle API")
        disp_response (response)
        raise SystemError("Error calling Moodle API")
    return response



class CourseList():
    """Class for list of all courses in Moodle and order them by id and idnumber."""
    def __init__(self):
        # TODO fullname atribute is filtered
        # (no <span class="multilang" lang="sl">)
        courses_data = call('core_course_get_courses', id=72)
        self.courses = []
        for data in courses_data:
            self.courses.append(Course(**data))
        self.id_dict = {}
        self.idnumber_dict = {}
        for course in self.courses:
            self.id_dict[course.id] = course
            if course.idnumber:
                self.idnumber_dict[course.idnumber] = course
    def __getitem__(self, key):
        if 0<= key < len(self.courses):
            return self.courses[key]
        else:
            raise IndexError

    def by_id(self, id):
        "Return course with given id."
        return self.id_dict.get(id)

    def by_idnumber(self, idnumber):
        "Course with given idnumber"
        return self.idnumber_dict.get(idnumber)

    def update_courses(courses_to_update, fields):
        "Update a list of courses in one go."
        if not ('id' in fields):
            fields.append('id')
        courses = [{k: c.__dict__[k] for k in fields} for c in courses_to_update]
        return call("core_course_update_courses",
             courses = courses)

class Course():
    """Class for a single course.

    Example: (id to retrieve in moodle's url)
    >>> Course (courseid=123)

    ## old example
    ## >>> Course(name="Example course", shortname="example", categoryid=1, courseid=123)
    """
    def __init__ (self, **data):
        self.__dict__.update(data)
        self.vpl_mods = None


    def pull_contents (self):
        resp = call ('core_course_get_contents', courseid = self.courseid, key = MoodleMobileKEY)
        self.__dict__.update({'sections': resp})


    def write_on_disk (self):
        info = os.path.join (BASEDIR_VPLS, INFO_FILE)
        print (self.courseid, "Writing Course information in", info)

        with open(info, 'w') as outfile:
            outfile.write (pretty_dict(self.__dict__))

    def read_from_disk (self):
        info = os.path.join (BASEDIR_VPLS, INFO_FILE)
        print ("Reading Course information for", self.courseid, "in", info)

        with open(info, 'r') as infile:
            data = json.load(infile)
            self.__dict__.update(data)


    def show_sections (self):
        for sec in self.sections:
            print ("id:", sec['id'], "=>", sec['name'])

    def vpl_modules (self):
        if not self.vpl_mods:
            self.vpl_mods = []
            for section in self.sections:
                for mod in section["modules"]:
                    if mod["modname"] == "vpl":
                        self.vpl_mods.append (mod)

        return self.vpl_mods

    def show_vpl_modules (self):
        mods = self.vpl_modules()
        for v in mods:
            print ("id:", v['id'], "=>", v['name'])




    def pull_vpls (self):
        """
        Pull all VPL from moodle and save them to disk
        """
        for v in self.vpl_modules():
            vpl = VPL(v['id'])
            vpl.pull_moodle()
            vpl.write_on_disk()


    def search_new_vpls(self):
        """
        Pull a clean version of the Caseine course and writes on disk
        """
        course_upd = Course(courseid = self.courseid)

        self.read_from_disk()
        print ("Updating course content from Caseine")
        course_upd.pull_contents ()

        print ("Writing course on local disk")
        course_upd.write_on_disk()

        print ("Searching for new VPL activities:")
        for v in course_upd.vpl_modules():

            if v in self.vpl_modules():
                continue

            print ("Found new VPL:", v['id'], v['name'])
            vpl = VPL(v['id'])

            vpl.pull_moodle()
            vpl.write_on_disk()

        print ("Now updated, please delete this course object")





    # Warning: Florent: functions below not tested
    def create (self):
        "Create this course on moodle"
        res = call('core_course_create_courses', courses = [self.__dict__])
        if type(res) == list:
            self.id = res[0].get('id')

    def update (self):
        "Update course"
        r = call('core_course_update_courses', courses = [self.__dict__])
    # End Warning

    def __str__ (self):
        return pretty_dict(self.__dict__)


class VPL():
    """
    Class for Virtual Programming Labs
    """

    def __init__(self, id):
        """
        id can be found in the URL of the VPL in the form "?id=XXXX"
        It can also be retrieved by scanning a course for all VPL modules.
        """
        self.id = id
        self.loaded = False
        self.local_path = None


    def evaluate (self):
        res = call ('mod_vpl_evaluate', id = self.id)
        disp_response (res)

        delay = 4
        print ("Waiting for", delay, "seconds")
        sleep(delay)

        res = call ('mod_vpl_get_result', id = self.id)

        if res["compilation"]:
            eprint ("Compilation error !")
            eprint (res["compilation"])
            eprint ("Compilation error !")
            return res

        disp_response (res)

        eva = res["evaluation"]
        print (eva)

        return res

    def pull_moodle (self):
        resp = call ('mod_vpl_info', key = VPLwebServiceKEY, id = self.id)
        self.__dict__.update(resp)
        self.loaded = True


    def push_moodle_type (self,t):
        if not self.loaded:
            eprint ("Cannot push on moodle, VPL is not loaded!")
            return

        fun = VPL_FILETYPES[t]

        files = self.__dict__[t]

        # if not any(files):
            # print ("No files", t, "to push to moodle")
            # return


        res = call (fun, key = VPLwebServiceKEY,
                id = self.id,
                files = self.__dict__[t]
                )
        if res == None:
            print ("Success")
        else:
            print ("Failed :-(")

    def push_moodle (self):
        for t in VPL_FILETYPES:
            self.push_moodle_type (t)


    def test_VPL_activity (self):
        """
        save on moodle using corrected files as regular submission and evaluate
        """

        print ("Testing VPL activity:", self.name)

        testfiles = self.reqfiles[:]       # shallow copy of list

        for c in self.corrfiles:
            name = c['name']

            for i,r in enumerate (testfiles):
                if r['name'] == name:
                    print ("Found duplicate file '", name, "', replacing with correction")
                    break
            testfiles[i] = c

        res = call ('mod_vpl_save', key = VPLwebServiceKEY,
            id = self.id,
            files = testfiles
            )

        disp_response(res)

        return self.evaluate()



    def get_dir(self):
        """
        Find the directory in the local hierarchy of this VPL
        """
        if not self.local_path:
            basepath = os.path.join (BASEDIR_VPLS, str(self.id))
            dirs = glob.glob (basepath + "*")
            if not dirs:
                eprint ("Error: Local hierarchy not available for VPL", self.id)
                return
            if len(dirs) > 1:
                eprint ("Error: Local hierarchy is ambiguous for VPL", self.id)
                eprint ("Possible directories are:")
                for d in dirs:
                    eprint ("\t", d)
                return

            path = dirs[0]
            self.local_path = path
        else:
            path = self.local_path
        return path


    def read_from_disk(self):
        """
        Read the local hierarchy to determine parameters & files
        """
        path = self.get_dir()
        info = os.path.join (path, INFO_FILE)
        print (self.id, "Reading information from", info)

        infodict = {}
        with open(info, 'r') as infile:
            data = json.load(infile)
            self.__dict__.update(data)

        for t in VPL_FILETYPES:
            print (self.id, "Reading files", t)
            fdir = os.path.join (path, t)
            if not os.path.exists(fdir):
                eprint ("Error: Directory not found:", fdir)
                continue

            l = []
            self.__dict__[t] = l

            for name in os.listdir (fdir):
                # skip backup and hidden files
                if name.endswith("~") or name.startswith(".") : continue
                fpath = os.path.join (fdir, name)
                print (self.id, "Reading file", fpath)
                with open (fpath, 'r') as infile:
                    data = infile.read ()
                l.append ({
                    'name': name,
                    'data': data
                    })
        self.loaded = True


    def write_on_disk(self):
        """
        Will save the VPL in a local hierarchy of directories

        TODO: remove from disk files that are not present in class.
        """

        if not self.loaded:
            eprint ("Cannot write on disk, VPL is not loaded!")
            return

        if not self.local_path:
            ## Dir with name or only id? That is the question
            # path = os.path.join (BASEDIR_VPLS, str(self.id) + "_" + self.name)
            path = os.path.join (BASEDIR_VPLS, str(self.id))
            self.local_path = path
        else:
            path = self.local_path

        if not os.path.exists(path):
            os.makedirs (path)

        info = os.path.join (path, INFO_FILE)
        print (self.id, "Writing information in", info)

        filetypes = []
        infodict = {}
        for k in self.__dict__:
            if k in VPL_FILETYPES:
                filetypes.append(k)
                continue
            infodict[k] = self.__dict__[k]

        with open(info, 'w') as outfile:
            outfile.write (pretty_dict(infodict))

        for t in filetypes:
            print (self.id, "Writing files", t)
            fdir = os.path.join (path, t)
            if not os.path.exists(fdir):
                os.makedirs (fdir)
            for afile in self.__dict__[t]:
                name = afile['name']
                fpath = os.path.join (fdir, name)
                print (self.id, "Writing file", fpath)
                with open (fpath, 'w') as outfile:
                    outfile.write (afile['data'])


    def update(self):
        self.pull_moodle()
        self.write_on_disk()

    def __str__ (self):
        return pretty_dict(self.__dict__)


class User():
    """Class for a single user.

    Example:
    >>> User(name="Janez", surname="Novak", email="janez.novak@student.si", username="jnovak", password="sila varno geslo")"""

    def __init__(self, **data):
        self.__dict__.update(data)

    def create(self):
        "Create new user on moodle site"
        valid_keys = ['username',
                      'firstname',
                      'lastname',
                      'email',
                      'auth',
                      'idnumber',
                      'password']
        values = {key: self.__dict__[key] for key in valid_keys}
        res = call('core_user_create_users', users = [values])
        if type(res) == list:
            self.id  = res[0].get('id')

    def update(self, field=None):
        "Upadte user data on moodle site"
        if field:
            values = {"id": self.id, field: self.__dict__[field]}
        else:
            values = self.__dict__
        r = call('core_user_update_users', users = [values])

    def get_by_field(self, field='username'):
        "Create new user if it does not exist, otherwise update data"
        res = call('core_user_get_users_by_field', field = field, values = [self.__dict__[field]])
        if (type(res) == list) and len(res) > 0:
            self.__dict__.update(res[0])
            return self
        else:
            return None
    def create_or_get_id(self):
        "Get Moodle id of the user or create one if it does not exists."
        if not self.get_by_field():
            self.create()

    def enroll(self, roleid=5):
        "Enroll users in courses with specific role"
        if len(self.courses)<=0:
            return None
        enrolments = []
        for course in self.courses:
            enrolments.append({'roleid': roleid, 'userid': self.id, 'courseid': course.id})
        r = call('enrol_manual_enrol_users', enrolments = enrolments)
        return r

    def enrolments(self, m_courses):
        "Get moodle courses, the user has to enroll"
        self.courses = []
        for idnumber in self.course_idnumbers:
            course = m_courses.by_idnumber(idnumber)
            if course:
                self.courses.append(course)
        return self.courses

class Cathegory():
    pass

class Enrolments():
    pass
